﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class Feature <T>: IReadable<T>, IWritable<T>
    {
        public T Read(string text)
        {
            return default(T);
        }

        public void Write(T line)
        {
            Console.WriteLine(line.ToString());
        }
    }
}
